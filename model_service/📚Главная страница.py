import streamlit as st



st.write("""
## Welcome to our Churn Prediction App 

This app predicts the **churn** in telekom!

Data obtained from the [website](https://www.ibm.com/docs/en/cognos-analytics/11.1.0?topic=samples-telco-customer-churn) by IBM.
""")

st.write("""
## Sandbox

Stands for model interpretation and customer probability calculation""")

st.write("""
## Reports

Vizualize drifts in new dataset based on reference dataset""")


st.write("""
## Marketing

Prepare your marketing campaign and enrich our monitoring""")

