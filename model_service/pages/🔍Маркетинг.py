import streamlit as st
import pickle
from io import BytesIO
import pandas as pd
import numpy as np
import psycopg
from evidently import ColumnMapping
from evidently.report import Report
from evidently.metrics import ColumnDriftMetric,  DatasetDriftMetric, DatasetMissingValuesMetric, \
ColumnQuantileMetric, DatasetCorrelationsMetric, ClassificationClassBalance, ClassificationQualityMetric
  
from evidently.metrics import ColumnDriftMetric, DatasetDriftMetric, DatasetMissingValuesMetric
import mlflow
import mlflow.sklearn
from dotenv import load_dotenv

load_dotenv()

model_name = 'churn_prediction'
stage = 'Staging'
version = "0.1.0."
class Model:

    def __init__(self, model_name, model_stage):
        self.model = mlflow.sklearn.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        preds = self.model.predict(data)
        return preds.tolist()

    def predict_proba(self, data):
        probs = self.model.predict_proba(data)
        return probs


model = Model(model_name, stage)
st.header('Interaction with customers')
st.write('1. Predict churn customers')
st.write('2. Interact with them')
st.write('3. Refresh new data for dashboard')
# data labeling
target = "Churn"
num_features = ["tenure", "MonthlyCharges", "TotalCharges"]
cat_features = [
    #'customerID',
    "Contract",
    "PaperlessBilling",
    "PaymentMethod",
    "gender",
    "SeniorCitizen",
    "Partner",
    "Dependents",
    "PhoneService",
    "MultipleLines",
    "InternetService",
    "OnlineSecurity",
    "OnlineBackup",
    "DeviceProtection",
    "TechSupport",
    "StreamingTV",
    "StreamingMovies",
]


column_mapping = ColumnMapping(
    prediction='prediction',
    numerical_features=num_features,
    categorical_features=cat_features,
    target=target
)

val_data = pd.read_csv('pages/data/processed/validation_data_pr2.csv') #.replace('missing', np.nan)


def to_excel(df):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')
    df.to_excel(writer, index=False, sheet_name='Sheet1')
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']
    format1 = workbook.add_format({'num_format': '0.00'})
    worksheet.set_column('A:A', None, format1)
    writer.save()
    processed_data = output.getvalue()
    return processed_data

date = st.sidebar.text_input('Input marketing campaign date', "2023-08-01")
date = pd.to_datetime(date ).date()
# Collects user input features into dataframe
    #st.write(input_df.head(2))
col1, col2 = st.columns(2)
with col1:
    uploaded_file = st.file_uploader("Upload CSV file for prediction", type=["csv"])
    if uploaded_file is not None:
        pred_df = pd.read_csv(uploaded_file).drop('Churn',1)
with col2:
    uploaded_file2 = st.file_uploader("Upload CSV file after interaction for refreshing", type=["csv"])

col1, col2 = st.columns(2)
with col1:
    if st.button('Getting churn probabilities'):
        st.write(
            "Wait for a while..."
        )
        
        preds = model.predict(pred_df)
        probs = model.predict_proba(pred_df)[:,1]
        pred_df['prediction'] = preds
        pred_df['probability'] = probs
        st.write(
            "Done"
        )
        df_xlsx = to_excel(pred_df)
        st.download_button(label='Download predictions',
                           data=df_xlsx,
                           file_name='Sample.xlsx')


with col2:
    if uploaded_file2 is not None:
        refhresh_df = pd.read_csv(uploaded_file2).replace('missing', np.nan)

          
        refhresh_df['prediction'] = model.predict(refhresh_df.drop('Churn', axis = 1))

    if st.button('Make dashboard great again'):
        st.write(
            "Wait for a while..."
            )

        report = Report(metrics=[
            ColumnDriftMetric(column_name='prediction'),
            DatasetDriftMetric(),
            DatasetMissingValuesMetric(),
            ClassificationQualityMetric(),
            ClassificationClassBalance(),
        
        ]
        )
        
        report.run(reference_data=val_data,
                   current_data=refhresh_df,
                   column_mapping=column_mapping)
        
        res1 = report.as_dict()
        prediction_drift = res1['metrics'][0]['result']['drift_score']
        dataset_drift = res1['metrics'][1]['result']['number_of_drifted_columns']
        
        missing_vals_current = res1['metrics'][2]['result']['current']['share_of_missing_values']
        missing_vals_reference = res1['metrics'][2]['result']['reference']['share_of_missing_values']
        
        accuracy_current = res1['metrics'][3]['result']['current']['accuracy']
        accuracy_reference = res1['metrics'][3]['result']['reference']['accuracy']
        
        f1_current = res1['metrics'][3]['result']['current']['f1']
        f1_reference = res1['metrics'][3]['result']['reference']['f1']
        
        recall_current = res1['metrics'][3]['result']['current']['recall']
        recall_reference = res1['metrics'][3]['result']['reference']['recall']
        
        precision_current = res1['metrics'][3]['result']['current']['precision']
        precision_reference = res1['metrics'][3]['result']['reference']['precision']
        with psycopg.connect("host=83.222.8.117 port=5432 user=root password=root dbname=test_db", autocommit=True) as conn:
            with conn.cursor() as curr:
                curr.execute(
                    "insert into dummy_metrics(date, prediction_drift, num_drifted_columns, missing_vals_current, missing_vals_reference, \
                    accuracy_current, accuracy_reference, f1_current, f1_reference, recall_current, recall_reference,\
                    precision_current, precision_reference) values (%s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s)",
                    (date, prediction_drift, dataset_drift, missing_vals_current, missing_vals_reference, accuracy_current,
                     accuracy_reference, f1_current, f1_reference, recall_current, recall_reference, precision_current,
                     precision_reference)
                )
        
        st.write(
            "Done"
        )
        
