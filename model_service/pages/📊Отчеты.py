import streamlit as st
import streamlit.components.v1 as components
import base64
import pickle
import pandas as pd
from evidently.metric_preset import ClassificationPreset, DataDriftPreset, DataQualityPreset, TargetDriftPreset
from evidently import ColumnMapping
from evidently.report import Report
import numpy as np
import mlflow
import mlflow.sklearn
from dotenv import load_dotenv
load_dotenv()

model_name = 'churn_prediction'
stage = 'Staging'
version = "0.1.0."
class Model:

    def __init__(self, model_name, model_stage):
        self.model = mlflow.sklearn.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        preds = self.model.predict(data)
        return preds.tolist()

    def predict_proba(self, data):
        probs = self.model.predict_proba(data)
        return probs


model = Model(model_name, stage)

val_data = pd.read_csv('pages/data/processed/validation_data_pr2.csv')

# data labeling
target = "Churn"
num_features = ["tenure", "MonthlyCharges", "TotalCharges"]
cat_features = [
    #'customerID',
    "Contract",
    "PaperlessBilling",
    "PaymentMethod",
    "gender",
    "SeniorCitizen",
    "Partner",
    "Dependents",
    "PhoneService",
    "MultipleLines",
    "InternetService",
    "OnlineSecurity",
    "OnlineBackup",
    "DeviceProtection",
    "TechSupport",
    "StreamingTV",
    "StreamingMovies",
]

val_preds = model.predict(val_data[num_features + cat_features])
val_data['prediction'] = val_preds

column_mapping = ColumnMapping(
    target='Churn',
    prediction='prediction',
    numerical_features=num_features,
    categorical_features=cat_features
)

report_name = st.sidebar.selectbox('Отчет', ['No report', 'DataDriftPreset','DataQualityPreset', 'TargetDriftPreset', 'ClassificationPreset'])

def make_report(ref_data, cur_data, report_name):
    if report_name == 'DataDriftPreset':
        report = Report(metrics=[
            DataDriftPreset()

        ]
        )
    elif report_name == 'DataQualityPreset':
        report = Report(metrics=[
            DataQualityPreset()

        ]
        )
    elif report_name == 'TargetDriftPreset':
        report = Report(metrics=[
            TargetDriftPreset()

        ]
        )
    elif report_name == 'ClassificationPreset':
        report = Report(metrics=[
            ClassificationPreset()

        ]
        )
    report.run(reference_data=ref_data,
               current_data=cur_data,
               column_mapping=column_mapping)
    
    report.save_html(f'reports/{report_name}.html')



st.title('Evidently reports')
st.write('1. Check your sample before you connect with people') 
st.write('2. Upload your file on the left panel') 
st.write('3. Choose a report') 


uploaded_file = st.sidebar.file_uploader("Upload CSV file", type=["csv"])
if uploaded_file is not None:
    test_data = pd.read_csv(uploaded_file).replace('missing',np.nan)
    test_preds = model.predict(test_data[num_features + cat_features])
    test_data['prediction'] = test_preds

    if report_name != 'No report':
        make_report(ref_data = val_data, cur_data = test_data, report_name = report_name)
        HtmlFile = open(f'reports/{report_name}.html', 'r', encoding = "utf-8")
        raw_html = HtmlFile.read().encode("utf-8")
        raw_html = base64.b64encode(raw_html).decode()
        components.iframe(f"data:text/html;base64,{raw_html}", height=15000, width=1000)
