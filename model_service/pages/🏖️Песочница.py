import streamlit as st
import numpy as np
import mlflow
import mlflow.sklearn
from dotenv import load_dotenv

import pandas as pd
import warnings

warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

load_dotenv()

model_name = 'churn_prediction'
stage = 'Staging'
version = "0.1.0."


class Model:
     
    def __init__(self, model_name, model_stage):
        self.model = mlflow.sklearn.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        preds = self.model.predict(data)
        return preds.tolist()
    def predict_proba(self, data):
        probs = self.model.predict_proba(data)
        return probs


model = Model(model_name, stage)

def user_input_features():
    st.sidebar.subheader('Billing data')
    tenure = st.sidebar.slider('Tenure (periods)', 0, 100, 44)
    MonthlyCharges = st.sidebar.slider('MonthlyCharges', 0, 200, 50)
    TotalCharges = st.sidebar.slider('TotalCharges', 0, 10000, 8000)
    Contract = st.sidebar.selectbox('Contract', ('Month-to-month',
                                                 'Two year',
                                                 'One year'))
    PaperlessBilling = st.sidebar.selectbox('PaperlessBilling', ('Yes', 'No'))
    PaymentMethod= st.sidebar.selectbox('PaymentMethod', ('Bank transfer (automatic)', 'Electronic check', 'Mailed check',
 'Credit card (automatic)'))

    st.sidebar.subheader('Personal data')
    gender = st.sidebar.selectbox('gender', ('Male', 'Female'))
    SeniorCitizen = st.sidebar.selectbox('SeniorCitizen', ('Yes', 'No'))

    Partner = st.sidebar.selectbox('Partner', ('Yes', 'No'))

    Dependents = st.sidebar.selectbox('Dependents', ('Yes', 'No'))
    st.sidebar.subheader('Technical data')
    PhoneService = st.sidebar.selectbox('PhoneService', ('Yes', 'No'))

    MultipleLines = st.sidebar.selectbox('MultipleLines', ('Yes', 'No','missing'))
    InternetService = st.sidebar.selectbox('InternetService', ('No', 'Fiber optic', 'DSL'))
    OnlineSecurity = st.sidebar.selectbox('OnlineSecurity', ('No', 'Yes', 'missing'))
    OnlineBackup = st.sidebar.selectbox('OnlineBackup', ('No', 'Yes', 'missing'))
    DeviceProtection = st.sidebar.selectbox('DeviceProtection', ('No', 'Yes', 'missing'))

    TechSupport = st.sidebar.selectbox('TechSupport', ('No', 'Yes', 'missing'))
    StreamingTV = st.sidebar.selectbox('StreamingTV', ('No', 'Yes', 'missing'))
    StreamingMovies = st.sidebar.selectbox('StreamingMovies', ('No', 'Yes', 'missing'))

    data = {
#'customerID':"customerID",
        'tenure': tenure,
            'MonthlyCharges':MonthlyCharges,
            'TotalCharges': TotalCharges,
            'Contract': Contract,
            'PaperlessBilling':PaperlessBilling,
            'PaymentMethod':PaymentMethod,
            'gender': gender,
            'SeniorCitizen': SeniorCitizen,
            'Partner': Partner,
            'Dependents': Dependents,
            'PhoneService': PhoneService,
            'MultipleLines' : MultipleLines,
            'InternetService' : InternetService,
            'OnlineSecurity': OnlineSecurity,
            'OnlineBackup': OnlineBackup,
            'DeviceProtection' : DeviceProtection,
            'TechSupport' : TechSupport,
            'StreamingTV': StreamingTV,
            'StreamingMovies': StreamingMovies}
    features = pd.DataFrame(data, index=[0])
    return features


st.write("""
# Churn Prediction App

This app predicts the **churn** in telekom!

Data obtained from the [website](https://www.ibm.com/docs/en/cognos-analytics/11.1.0?topic=samples-telco-customer-churn) by IBM.
""")


# Collects user input features into dataframe
uploaded_file = st.sidebar.file_uploader("Upload your input CSV file", type=["csv"])
if uploaded_file is not None:
    input_df = pd.read_csv(uploaded_file)
else:
    input_df = user_input_features()


df = input_df.drop(columns=['Churn', 'customerID'])
df = df[:1] # Selects only the first row (the user input data)

# Displays the user input features
st.subheader('User Input features')

if uploaded_file is not None:
    st.write(df)
    pass
else:
    st.write('Params in table')
    st.write(df)

prediction = model.predict(df)
prediction_proba = model.predict_proba(df)
prediction_proba = pd.DataFrame(prediction_proba, columns = ['No','Yes'])
def color_survived(val):
    color = 'red' if val > 0.6 else 'yellow' if val > 0.4 else 'green'
    return f'background-color: {color}'

col1, col2 = st.columns(2)

with col1:
    penguins_species = np.array(['No','Yes'])
    st.subheader('Клиент склонен к оттоку?')
    st.write(penguins_species[prediction][0])

with col2 :
    st.subheader('Prediction Probability')
    st.write(prediction_proba.style.applymap(color_survived, subset=['No','Yes']))
